--Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:29:27 MST 2014
--Date        : Fri Oct 30 16:43:59 2015
--Host        : Jim-HP-Pavilion running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target BasicControlBD_wrapper.bd
--Design      : BasicControlBD_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity BasicControlBD_wrapper is
  port (
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    Sensor_DRDY : in STD_LOGIC_VECTOR ( 3 downto 0 );
    btns_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    iic_rtl_0_scl_io : inout STD_LOGIC;
    iic_rtl_0_sda_io : inout STD_LOGIC;
    iic_rtl_1_scl_io : inout STD_LOGIC;
    iic_rtl_1_sda_io : inout STD_LOGIC;
    iic_rtl_2_scl_io : inout STD_LOGIC;
    iic_rtl_2_sda_io : inout STD_LOGIC;
    iic_rtl_scl_io : inout STD_LOGIC;
    iic_rtl_sda_io : inout STD_LOGIC;
    leds_8bits_tri_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    sws_8bits_tri_i : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end BasicControlBD_wrapper;

architecture STRUCTURE of BasicControlBD_wrapper is
  component BasicControlBD is
  port (
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    iic_rtl_scl_i : in STD_LOGIC;
    iic_rtl_scl_o : out STD_LOGIC;
    iic_rtl_scl_t : out STD_LOGIC;
    iic_rtl_sda_i : in STD_LOGIC;
    iic_rtl_sda_o : out STD_LOGIC;
    iic_rtl_sda_t : out STD_LOGIC;
    iic_rtl_0_scl_i : in STD_LOGIC;
    iic_rtl_0_scl_o : out STD_LOGIC;
    iic_rtl_0_scl_t : out STD_LOGIC;
    iic_rtl_0_sda_i : in STD_LOGIC;
    iic_rtl_0_sda_o : out STD_LOGIC;
    iic_rtl_0_sda_t : out STD_LOGIC;
    iic_rtl_1_scl_i : in STD_LOGIC;
    iic_rtl_1_scl_o : out STD_LOGIC;
    iic_rtl_1_scl_t : out STD_LOGIC;
    iic_rtl_1_sda_i : in STD_LOGIC;
    iic_rtl_1_sda_o : out STD_LOGIC;
    iic_rtl_1_sda_t : out STD_LOGIC;
    iic_rtl_2_scl_i : in STD_LOGIC;
    iic_rtl_2_scl_o : out STD_LOGIC;
    iic_rtl_2_scl_t : out STD_LOGIC;
    iic_rtl_2_sda_i : in STD_LOGIC;
    iic_rtl_2_sda_o : out STD_LOGIC;
    iic_rtl_2_sda_t : out STD_LOGIC;
    sws_8bits_tri_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    btns_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    leds_8bits_tri_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    Sensor_DRDY : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  end component BasicControlBD;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal iic_rtl_0_scl_i : STD_LOGIC;
  signal iic_rtl_0_scl_o : STD_LOGIC;
  signal iic_rtl_0_scl_t : STD_LOGIC;
  signal iic_rtl_0_sda_i : STD_LOGIC;
  signal iic_rtl_0_sda_o : STD_LOGIC;
  signal iic_rtl_0_sda_t : STD_LOGIC;
  signal iic_rtl_1_scl_i : STD_LOGIC;
  signal iic_rtl_1_scl_o : STD_LOGIC;
  signal iic_rtl_1_scl_t : STD_LOGIC;
  signal iic_rtl_1_sda_i : STD_LOGIC;
  signal iic_rtl_1_sda_o : STD_LOGIC;
  signal iic_rtl_1_sda_t : STD_LOGIC;
  signal iic_rtl_2_scl_i : STD_LOGIC;
  signal iic_rtl_2_scl_o : STD_LOGIC;
  signal iic_rtl_2_scl_t : STD_LOGIC;
  signal iic_rtl_2_sda_i : STD_LOGIC;
  signal iic_rtl_2_sda_o : STD_LOGIC;
  signal iic_rtl_2_sda_t : STD_LOGIC;
  signal iic_rtl_scl_i : STD_LOGIC;
  signal iic_rtl_scl_o : STD_LOGIC;
  signal iic_rtl_scl_t : STD_LOGIC;
  signal iic_rtl_sda_i : STD_LOGIC;
  signal iic_rtl_sda_o : STD_LOGIC;
  signal iic_rtl_sda_t : STD_LOGIC;
begin
BasicControlBD_i: component BasicControlBD
    port map (
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      Sensor_DRDY(3 downto 0) => Sensor_DRDY(3 downto 0),
      btns_5bits_tri_i(4 downto 0) => btns_5bits_tri_i(4 downto 0),
      iic_rtl_0_scl_i => iic_rtl_0_scl_i,
      iic_rtl_0_scl_o => iic_rtl_0_scl_o,
      iic_rtl_0_scl_t => iic_rtl_0_scl_t,
      iic_rtl_0_sda_i => iic_rtl_0_sda_i,
      iic_rtl_0_sda_o => iic_rtl_0_sda_o,
      iic_rtl_0_sda_t => iic_rtl_0_sda_t,
      iic_rtl_1_scl_i => iic_rtl_1_scl_i,
      iic_rtl_1_scl_o => iic_rtl_1_scl_o,
      iic_rtl_1_scl_t => iic_rtl_1_scl_t,
      iic_rtl_1_sda_i => iic_rtl_1_sda_i,
      iic_rtl_1_sda_o => iic_rtl_1_sda_o,
      iic_rtl_1_sda_t => iic_rtl_1_sda_t,
      iic_rtl_2_scl_i => iic_rtl_2_scl_i,
      iic_rtl_2_scl_o => iic_rtl_2_scl_o,
      iic_rtl_2_scl_t => iic_rtl_2_scl_t,
      iic_rtl_2_sda_i => iic_rtl_2_sda_i,
      iic_rtl_2_sda_o => iic_rtl_2_sda_o,
      iic_rtl_2_sda_t => iic_rtl_2_sda_t,
      iic_rtl_scl_i => iic_rtl_scl_i,
      iic_rtl_scl_o => iic_rtl_scl_o,
      iic_rtl_scl_t => iic_rtl_scl_t,
      iic_rtl_sda_i => iic_rtl_sda_i,
      iic_rtl_sda_o => iic_rtl_sda_o,
      iic_rtl_sda_t => iic_rtl_sda_t,
      leds_8bits_tri_o(7 downto 0) => leds_8bits_tri_o(7 downto 0),
      sws_8bits_tri_i(7 downto 0) => sws_8bits_tri_i(7 downto 0)
    );
iic_rtl_0_scl_iobuf: component IOBUF
    port map (
      I => iic_rtl_0_scl_o,
      IO => iic_rtl_0_scl_io,
      O => iic_rtl_0_scl_i,
      T => iic_rtl_0_scl_t
    );
iic_rtl_0_sda_iobuf: component IOBUF
    port map (
      I => iic_rtl_0_sda_o,
      IO => iic_rtl_0_sda_io,
      O => iic_rtl_0_sda_i,
      T => iic_rtl_0_sda_t
    );
iic_rtl_1_scl_iobuf: component IOBUF
    port map (
      I => iic_rtl_1_scl_o,
      IO => iic_rtl_1_scl_io,
      O => iic_rtl_1_scl_i,
      T => iic_rtl_1_scl_t
    );
iic_rtl_1_sda_iobuf: component IOBUF
    port map (
      I => iic_rtl_1_sda_o,
      IO => iic_rtl_1_sda_io,
      O => iic_rtl_1_sda_i,
      T => iic_rtl_1_sda_t
    );
iic_rtl_2_scl_iobuf: component IOBUF
    port map (
      I => iic_rtl_2_scl_o,
      IO => iic_rtl_2_scl_io,
      O => iic_rtl_2_scl_i,
      T => iic_rtl_2_scl_t
    );
iic_rtl_2_sda_iobuf: component IOBUF
    port map (
      I => iic_rtl_2_sda_o,
      IO => iic_rtl_2_sda_io,
      O => iic_rtl_2_sda_i,
      T => iic_rtl_2_sda_t
    );
iic_rtl_scl_iobuf: component IOBUF
    port map (
      I => iic_rtl_scl_o,
      IO => iic_rtl_scl_io,
      O => iic_rtl_scl_i,
      T => iic_rtl_scl_t
    );
iic_rtl_sda_iobuf: component IOBUF
    port map (
      I => iic_rtl_sda_o,
      IO => iic_rtl_sda_io,
      O => iic_rtl_sda_i,
      T => iic_rtl_sda_t
    );
end STRUCTURE;
